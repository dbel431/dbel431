(function(){
  angular.module('todoApp')
  .controller('TodoListController',TodoListController);
  TodoListController.$inject=['appData'];
  function TodoListController(appData) {
    var vm=this;
    vm.data={
      init:init,
      dis:dis,
      show:'line',
      initData:{}
    };
    vm.data.init();
    function dis(data)
    {
      vm.data.show=data;
    }
    function init()
    {
      vm.data.initData=appData.getData(); 
    }
  }
})();
