(function(){
 angular.module('todoApp')
 .factory("appData", function(){
      var data={};
      data.getData=getData;
      function getData(){  
            return  {labels:["January", "February", "March", "April", "May", "June", "July","aug"],
            series:['Series A', 'Series B','Series C'],
            data:[
            [90,35,50,96,50,20,500,450],
            [15,25,35,45,55,65,75,150],
            [10,45,90,26,70,20,45,200]
            ]};
      }
      return data;
});
})();