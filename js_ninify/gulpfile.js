var gulp         = require('gulp');
// var less         = require('gulp-less');
// var jshint       = require('gulp-jshint');
// var ngTemplates  = require('gulp-angular-templatecache');
var rename       = require('gulp-rename');
var uglify       = require('gulp-uglify');
var concat       = require('gulp-concat');
// var minifyCss    = require('gulp-minify-css');

// var del          = require('del');
// var stylish      = require('jshint-stylish');

gulp.task('scripts-dev', function() {
  gulp.src([
  	'public/scripts/third-party/jquery-1.11.3.min.js',
  	'public/scripts/third-party/angular.min.js',
  	'public/scripts/third-party/angular-ui-router.js',
  	'public/scripts/third-party/xeditable.js',
  	'public/scripts/third-party/spin.min.js',
  	'public/scripts/third-party/angular-spinner.js',
  	'public/scripts/third-party/ui-bootstrap-tpls-0.13.3.min.js',
  	'public/scripts/third-party/angular-animate.min.js',
  	'public/scripts/third-party/bootstrap-switch.min.js',
  	'public/scripts/third-party/jquery-ui.js',
  	'public/scripts/third-party/select.min.js',
  	'public/scripts/third-party/focusif.js',
  	'public/scripts/third-party/Chart.min.js',
  	'public/scripts/third-party/angular-chart.js',
  	'public/scripts/third-party/hotkeys.min.js',
  	'public/scripts/third-party/bootstrap.min.js',
  	'public/scripts/third-party/ng-csv.min.js',
  	'public/scripts/third-party/angular-sanitize.min.js',
  	'public/scripts/third-party/isteven-multi-select.js',
  	'public/scripts/third-party/angucomplete.js',
  	'public/scripts/third-party/angular-toastr.tpls.min.js',
  	'public/scripts/third-party/multiple.js',
  	'public/scripts/third-party/nprogress.js',
  	'public/scripts/third-party/toastr.js',
  	'public/scripts/third-party/toastr.tpl.js',
  	'public/scripts/third-party/angular-permission.js',
  	'public/scripts/third-party/ngDialog.min.js',
  	'public/scripts/third-party/date.js',
  	'public/scripts/third-party/bootstrap-select.min.js',
  	'public/scripts/third-party/jquery.selectBox.js',
  'public/scripts/third-party/jquery.stickyheader.js',
  'public/scripts/third-party/jquery.multiple.select.js',
  'public/scripts/third-party/bootstrap-multiselect.js',
  'public/scripts/third-party/bootstrap-multiselect-collapsible-groups.js',
  'http://cdnjs.cloudflare.com/ajax/libs/lodash.js/2.4.1/lodash.min.js',
  'public/scripts/third-party/angularjs-dropdown-multiselect.js',
  'public/scripts/third-party/d3.min.js',
  'public/scripts/third-party/nv.d3.min.js',
  'public/scripts/third-party/angular-nvd3.js',
  	 'public/scripts/app/*.js',
  	 'public/scripts/app/**/*.js'
  	 ])
	.pipe(concat('mediplus.js'))
    .pipe(gulp.dest('public/scripts/build'))
    .pipe(uglify({mangle: false}))
    .pipe(rename('mediplus.min.js'))
    .pipe(gulp.dest('public/scripts/build'));
});

gulp.task('clean', function(cb) {
  // del(['public/scripts/mediplus/dist'], cb);
});

gulp.task('default', ['clean', 'scripts-dev' ]);
